// plusCourtChemin.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.

#define _SECURE_SCL 0
#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
#define NOMINMAX

#define _SILENCE_CXX17_OLD_ALLOCATOR_MEMBERS_DEPRECATION_WARNING
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
using namespace std;


const int NB_SOMMET = 9;
const int MAX_VALUE = 9999;
const int MAX_SUCC = 3;
const int MAX_PIECE = 3;
const int MAX_OPE = 3;
const int MAX_MACHINE = 3;

typedef struct t_couple {
	int x;
	int y;
}T_couple;

typedef struct t_probleme {
	int nbMachine;
	int nbPiece;
	int pieceOperation[MAX_PIECE][MAX_OPE];
	int machineOperation[MAX_MACHINE][MAX_OPE];
}T_probleme;

typedef struct t_solution_tp2 {
	int date[MAX_PIECE][MAX_OPE];
	int cout;
	T_couple chemin[MAX_MACHINE][MAX_OPE];
}T_solution_tp2;

typedef struct t_solution {
	int m[NB_SOMMET];
	int pere[NB_SOMMET];
}T_solution;

typedef struct t_liste {
	int nbSuc;
	int liste_succ[NB_SOMMET];
	int liste_val[NB_SOMMET];
}T_liste;

typedef struct graphe {
	int nbSom;
	T_liste liste[NB_SOMMET];
}Graphe;

void afficherGraphe(Graphe graphe) {
	int i, j;
	std::cout << "Nombre de sommet: " << graphe.nbSom << "\n";
	for (i = 0; i < NB_SOMMET; i++) {
		std::cout << "Sommet " << i + 1 << ":";
		if (graphe.liste[i].nbSuc > 0) {
			std::cout << "\n\tNombre de successeur: " << graphe.liste[i].nbSuc;
			for (j = 0; j < graphe.liste[i].nbSuc; j++) {
				std::cout << "\n\tSuccesseur: " << graphe.liste[i].liste_succ[j] + 1 << " -> " << graphe.liste[i].liste_val[j];
			}
		}
		else
			std::cout << "\n\tAucun successeur!";
		std::cout << "\n";
	}
	std::cout << "Fin Graphe\n";
}

void creationGraphe(Graphe& graphe) {
	graphe.nbSom = NB_SOMMET;
	// Sommet 1 :
	graphe.liste[0].nbSuc = 2;
	graphe.liste[0].liste_succ[0] = 3;
	graphe.liste[0].liste_val[0] = 4;
	graphe.liste[0].liste_succ[1] = 1;
	graphe.liste[0].liste_val[1] = 10;
	// Sommet 2 :
	graphe.liste[1].nbSuc = 2;
	graphe.liste[1].liste_succ[0] = 2;
	graphe.liste[1].liste_val[0] = 1;
	graphe.liste[1].liste_succ[1] = 4;
	graphe.liste[1].liste_val[1] = 2;
	// Sommet 3 :
	graphe.liste[2].nbSuc = 2;
	graphe.liste[2].liste_succ[0] = 5;
	graphe.liste[2].liste_val[0] = 4;
	graphe.liste[2].liste_succ[1] = 7;
	graphe.liste[2].liste_val[1] = 2;
	// Sommet 4 :
	graphe.liste[3].nbSuc = 1;
	graphe.liste[3].liste_succ[0] = 5;
	graphe.liste[3].liste_val[0] = 12;
	// Sommet 5 :
	graphe.liste[4].nbSuc = 2;
	graphe.liste[4].liste_succ[0] = 2;
	graphe.liste[4].liste_val[0] = 9;
	graphe.liste[4].liste_succ[1] = 7;
	graphe.liste[4].liste_val[1] = 3;
	// Sommet 6 :
	graphe.liste[5].nbSuc = 1;
	graphe.liste[5].liste_succ[0] = 6;
	graphe.liste[5].liste_val[0] = 5;
	// Sommet 7 :
	graphe.liste[6].nbSuc = 2;
	graphe.liste[6].liste_succ[0] = 7;
	graphe.liste[6].liste_val[0] = 3;
	graphe.liste[6].liste_succ[1] = 8;
	graphe.liste[6].liste_val[1] = 3;
	// Sommet 8 :
	graphe.liste[7].nbSuc = 1;
	graphe.liste[7].liste_succ[0] = 8;
	graphe.liste[7].liste_val[0] = 3;
	// Sommet 9 :
	graphe.liste[8].nbSuc = 0;
}

void chargementGraphe(Graphe& graphe) {
	int nbsommet, sommet, nbsuc, succ, val, i;
	ifstream monFlux("Graphe.txt");
	if (monFlux.is_open() != true) {
		std::cout << "Graphe chargé depuis le Stub.\n";
		creationGraphe(graphe);
		return;
	}
	monFlux >> nbsommet;
	graphe.nbSom = nbsommet;
	while (nbsommet > 0) {
		monFlux >> sommet;
		monFlux >> nbsuc;
		graphe.liste[sommet].nbSuc = nbsuc;
		for (i = 0; i < nbsuc; i++) {
			monFlux >> succ;
			monFlux >> val;
			graphe.liste[sommet].liste_succ[i] = succ;
			graphe.liste[sommet].liste_val[i] = val;
		}
		nbsommet--;
	}
	monFlux.close();
	std::cout << "Graphe chargé depuis le fichier text.\n";
}

void chargementVecteur() {
	T_probleme probleme;
	int i;
	ifstream monFlux("storage.txt");
	if (monFlux.is_open() != true) {
		std::cout << "Fichier non trouvé !" << endl;
		return;
	}
	monFlux >> probleme.nbPiece;
	monFlux >> probleme.nbMachine;
	
	monFlux.close();
}

void affichageDIJSTRA(T_solution solution, int traite[]) {
	int i;
	std::cout << "M\tT\tPERE\n";
	for (i = 0; i < NB_SOMMET; i++) {
		std::cout << solution.m[i] << "\t" << traite[i] << "\t" << solution.pere[i] << "\n";
	}
}

void initialiserDIJSTRA(Graphe graphe, T_solution& solution, int traite[], int sommetDepart) {
	int i;
	for (i = 0; i < NB_SOMMET; i++) {
		solution.m[i] = MAX_VALUE;
		solution.pere[i] = -1;
		traite[i] = 0;
		if (i == sommetDepart - 1) {
			solution.m[i] = 0;
		}
	}
}

int rechercherSommetDIJSTRA(T_solution solution, int traite[]) {
	int i, valeurMin = MAX_VALUE, emplacement = 0;
	for (i = 0; i < NB_SOMMET; i++) {
		if (solution.m[i] < valeurMin && traite[i] != 1) {
			valeurMin = solution.m[i];
			emplacement = i;
		}
	}
	return emplacement;
}

void calculMarqueDIJSTRA(Graphe graphe, T_solution& solution, int sommet) {
	int i;
	for (i = 0; i < graphe.liste[sommet].nbSuc; i++) {
		if (solution.m[graphe.liste[sommet].liste_succ[i]] > (solution.m[sommet] + graphe.liste[sommet].liste_val[i])) {
			solution.m[graphe.liste[sommet].liste_succ[i]] = solution.m[sommet] + graphe.liste[sommet].liste_val[i];
			solution.pere[graphe.liste[sommet].liste_succ[i]] = sommet + 1;
		}
	}
}

void solutionDIJSTRA(T_solution& solution, int sommetDepart) {
	int sommet = 8;
	std::cout << "Le parcours est donc : " << (sommet + 1);
	while (solution.pere[sommet] != -1) {
		sommet = solution.pere[sommet] - 1;
		std::cout << " <- " << (sommet + 1);
	}
	std::cout << "\n";
}

void appliquerDIJSTRA(Graphe graphe, T_solution& solution, int sommetDepart) {
	bool run = true;
	int sommet, i;
	sommetDepart;
	int traite[NB_SOMMET];
	initialiserDIJSTRA(graphe, solution, traite, sommetDepart);
	affichageDIJSTRA(solution, traite);

	while (run) {
		run = false;
		for (i = 0; i < NB_SOMMET; i++) {
			if (traite[i] == 0) {
				run = true;
				break;
			}
		}
		sommet = rechercherSommetDIJSTRA(solution, traite);
		std::cout << "Le sommet le plus faible est à la position : " << sommet << " du tableau.\n";
		calculMarqueDIJSTRA(graphe, solution, sommet);
		traite[sommet] = 1;
		affichageDIJSTRA(solution, traite);
	};
	solutionDIJSTRA(solution, sommetDepart);
}

void afficherOrdre(int* ordre) {
	int i;

	std::cout << "Ordre de la liste : ";
	for (i = 0; i < NB_SOMMET; i++) {
		std::cout << " / " << ordre[i];
	}

	std::cout << "\n";
}

void affichageBELLMAN(T_solution solution) {
	int i;
	std::cout << "M\tPERE\n";
	for (i = 0; i < NB_SOMMET; i++) {
		std::cout << solution.m[i] << "\t" << solution.pere[i] << "\n";
	}
}

void initialiserSommetModif(int* liste) {
	int i;

	for (i = 0; i < NB_SOMMET; i++) {
		liste[i] = 0;
	}
}

void afficherSommetModif(int* liste) {
	int i;
	bool trouve = false;

	for (i = 0; i < NB_SOMMET; i++) {
		if (liste[i] != 0) {
			trouve = true;
			std::cout << i + 1 << " / ";
		}
	}
	if (trouve)
		std::cout << " : sommets qui ont étaient modifiés. Il faut donc reboucler.\n";
	else
		std::cout << "Aucun sommet modifié. Fin de la recherche.\n";
}

void choixOrdre(int* ordre) {
	int test[NB_SOMMET] = { 6,5,4,3,8,7,2,1,0 }, i;
	//0,1,2,7,8,3,4,5,6
	//6,5,4,3,8,7,2,1,0
	//1,3,5,7,8,6,4,2,0
	for (i = 0; i < NB_SOMMET; i++) {
		ordre[i] = test[i];
	}
}

void initialiserBELLMAN(Graphe graphe, T_solution& solution, int* sommets, int* ordre, int sommetDepart) {
	int i;
	for (i = 0; i < NB_SOMMET; i++) {
		solution.m[i] = MAX_VALUE;
		solution.pere[i] = -1;
		if (i == sommetDepart - 1) {
			solution.m[i] = 0;
		}
	}
	choixOrdre(ordre);
	initialiserSommetModif(sommets);
}

void solutionBELLMAN(T_solution solution) {
	int sommet = 8;

	affichageBELLMAN(solution);
	std::cout << "Le parcours est donc : " << (sommet + 1);
	sommet = solution.pere[sommet] - 1;
	while (sommet != -1) {
		std::cout << " <- " << (sommet + 1);
		if (sommet == 0)
			break;
		sommet = solution.pere[sommet] - 1;
	}
	std::cout << "\n";
}

void descriptionBELLMAN(Graphe graphe, int sommet, int nbSommetModif) {
	std::cout << "Etape du sommet " << sommet << ":\n\tIl possède " << graphe.liste[sommet].nbSuc << " sommets. (" << nbSommetModif << "/" << graphe.liste[sommet].nbSuc << " mis à jour)\n";
}

void appliquerBELLMAN(Graphe graphe, T_solution solution, int sommetDepart) {
	int ordre[NB_SOMMET], i, j, sommet, sommetModif[NB_SOMMET], nbSommetModif = 0;
	bool arret = false;

	//std::cout << "coucou";
	initialiserBELLMAN(graphe, solution, sommetModif, ordre, sommetDepart);
	afficherOrdre(ordre);
	affichageBELLMAN(solution);

	while (!arret) {
		arret = true;
		initialiserSommetModif(sommetModif);
		for (i = 0; i < NB_SOMMET; i++) {
			sommet = ordre[i];
			nbSommetModif = 0;
			for (j = 0; j < graphe.liste[sommet].nbSuc; j++) {
				if (solution.m[graphe.liste[sommet].liste_succ[j]] > (solution.m[sommet] + graphe.liste[sommet].liste_val[j])) {
					solution.m[graphe.liste[sommet].liste_succ[j]] = solution.m[sommet] + graphe.liste[sommet].liste_val[j];
					solution.pere[graphe.liste[sommet].liste_succ[j]] = sommet + 1;
					arret = false;
					sommetModif[sommet] = 1;
					nbSommetModif++;
				}
			}
			descriptionBELLMAN(graphe, sommet, nbSommetModif);
		}
		afficherSommetModif(sommetModif);
	}
	solutionBELLMAN(solution);
}

int main() {
	/*Graphe graphe;
	T_solution solution;

	std::cout << "Partie 1:";

	chargementGraphe(graphe);
	afficherGraphe(graphe);

	std::cout << "\n\tExercice 1: Méthode de Dijstra\n\n";

	appliquerDIJSTRA(graphe, solution, 1);

	std::cout << "\n\n\tExercice 2: Méthode de Bellman\n\n";

	appliquerBELLMAN(graphe, solution, 1);

	std::cout << "\n\n\n\n";*/

	chargementVecteur();

	return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer :
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
